# NPX Test

This repository reproduces an issue when using [`npx`](https://www.npmjs.com/package/npx) to execute scripts from a package published to a GitLab registry.

This project includes two extra branches:

- `no-dependencies`, which is published at version [`1.0.0` of `@testing-npm-registry/npx-test`](https://gitlab.com/testing-npm-registry/npx-test/-/packages)
  - https://gitlab.com/testing-npm-registry/npx-test/tree/no-dependencies
- `with-dependencies`, which is published at version [`2.0.0` of `@testing-npm-registry/npx-test`](https://gitlab.com/testing-npm-registry/npx-test/-/packages)
  - https://gitlab.com/testing-npm-registry/npx-test/tree/with-dependencies

## Reproducing the problem

1. Add the follow line to your root `.npmrc` (i.e. `~/.npmrc`) to point the `@testing-npm-registry` scope to GitLab's registry:

   ```
   @testing-npm-registry:registry=https://gitlab.com/api/v4/packages/npm/
   ```

1. Run `npx @testing-npm-registry/npx-test@1`. This will execute the `say-hello` script in version `1.0.0` of `@testing-npm-registry/npx-test`, with does not contain any dependencies. This should execute successfully:

   ```
   ~/source/npx-test $ npx @testing-npm-registry/npx-test@1
   npx: installed 1 in 1.368s
   Hello World!
   ```

1. Run `npx @testing-npm-registry/npx-test@2`. This will execute the `say-hello` script in version `2.0.0` of `@testing-npm-registry/npx-test`, with contains one dependency ([`moment`](https://www.npmjs.com/package/moment)). This command will **fail** with the following output:

   ```
   ~/source/npx-test $ npx @testing-npm-registry/npx-test@2
   npx: installed 1 in 1.116s
   Cannot find module 'moment'
   Require stack:
   - /Users/nathanfriend/.npm/_npx/89425/lib/node_modules/@testing-npm-registry/npx-test/cli.js
   ```

   If this command was working properly, you would instead see:

   ```
   ~/source/npx-test $ npx @testing-npm-registry/npx-test@2
   npx: installed 2 in 1.838s
   Hello World!
   Today is October 30, 2019
   ```

## A workaround

One possible workaround is to reference the package by URL instead of using the scoped package name. Using this workaround, it's not necessary to edit your `.npmrc` as described above.

```
~/source/npx-test $ npx https://gitlab.com/testing-npm-registry/npx-test.git#with-dependencies
npx: installed 2 in 1.838s
Hello World!
Today is October 31, 2019
```

Note: the `#with-dependencies` section of the URL above instructs `npx` to use the `with-dependencies` branch of the repo at https://gitlab.com/testing-npm-registry/npx-test.git.

## Root cause

This is _most likely_ an issue with `npx`, not GitLab's registry implementation. However, this requires a little more investigation to be sure.

An issue has been opened on `npx`'s GitHub repo here: https://github.com/npm/npx/issues/16
